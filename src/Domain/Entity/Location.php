<?php

namespace App\Domain\Entity;

class Location {
    private int $id;
    private \DateTime $debut;
    private \DateTime $fin;
    private int $note;
    private string $commentaire;
    private Utilisatrice $utilisatrice;
    private Velo $velo;

    public function __construct(Utilisatrice $utilisatrice, Velo $velo,\DateTime $debut, \DateTime $fin = null, int $note = null, string $commentaire =null, int $id =null) {
        $this->debut = $debut;
        $this->fin = $fin;
        $this->note = $note;
        $this->utilisatrice = $utilisatrice;
        $this->velo = $velo;
        $this->commentaire = $commentaire;
        $this->id = $id;
    }

    public function finirLocation(\DateTime $fin, ?int $note, ?string $commentaire){
        $this->fin = $fin;
        if($note >= 0 && $note <= 5) {
            $this->note = $note;
        }
        $this->commentaire = $commentaire;

    }
    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of debut
     */ 
    public function getDebut()
    {
        return $this->debut;
    }

    /**
     * Get the value of fin
     */ 
    public function getFin()
    {
        return $this->fin;
    }

    /**
     * Get the value of note
     */ 
    public function getNote()
    {
        return $this->note;
    }

    /**
     * Get the value of commentaire
     */ 
    public function getCommentaire()
    {
        return $this->commentaire;
    }

    /**
     * Get the value of utilisatrice
     */ 
    public function getUtilisatrice()
    {
        return $this->utilisatrice;
    }

    /**
     * Get the value of velo
     */ 
    public function getVelo()
    {
        return $this->velo;
    }
}