<?php

namespace App\Domain\Entity;

class Station {
    private int $id;
    private array $localisation;
    private array $velos;
    private int $places;
    
    public function __construct(float $x, float $y, int $places, $id = null, $velos = []) {
        $this->localisation = [
            'x' => $x,
            'y' => $y
        ];
        $this->places = $places;
        $this->id = $id;
        $this->velos = $velos;
    }

    

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of localisation
     */ 
    public function getLocalisation()
    {
        return $this->localisation;
    }

    /**
     * Get the value of places
     */ 
    public function getPlaces()
    {
        return $this->places;
    }

    /**
     * Get the value of velos
     */ 
    public function getVelos()
    {
        return $this->velos;
    }
    public function velosDisponibles():int {
        $count = 0;
        foreach($this->velos as $velo) {
            if($velo->isDisponible() && !$velo->isHs()) {
                $count++;
            }
        }
        return $count;
    }
    public function placesDisponibles():int
    {
        return $this->places - count($this->velos);
    }


    public function retirerVelo(int $index):Velo {
        $velo = array_splice($this->velos, $index, 1);
        return $velo[0];
    }

    public function restituerVelo(Velo $velo, int $index) {
        if(!$this->velos[$index]) {
            $this->velos[$index] = $velo;

        }
    }
}