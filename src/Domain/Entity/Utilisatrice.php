<?php

namespace App\Domain\Entity;

class Utilisatrice {
    private int $id;
    private string $login;
    private string $mdp;

    public function __construct($login, $mdp, $id = null) {
        $this->login = $login;
        $this->mdp = $mdp;
        $this->id = $id;
    }
    

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of login
     */ 
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * Get the value of mdp
     */ 
    public function getMdp()
    {
        return $this->mdp;
    }
}