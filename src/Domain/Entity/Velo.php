<?php

namespace App\Domain\Entity;

class Velo {
    private int $id;
    private bool $disponible;
    private bool $hs;
    private int $note;

    public function __construct(int $note = 5, $id = null, bool $disponible = true, bool $hs = false) {
        $this->id = $id;
        $this->disponible = $disponible;
        $this->hs = $hs;
        $this->note = $note;
    }

    /**
     * Get the value of id
     */ 
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get the value of disponible
     */ 
    public function isDisponible()
    {
        return $this->disponible;
    }

    /**
     * Get the value of hs
     */ 
    public function isHs()
    {
        return $this->hs;
    }

    /**
     * Get the value of note
     */ 
    public function getNote()
    {
        return $this->note;
    }

    public function toggleHs() {
        $this->hs = !$this->hs;
    }
    public function toggleDisponible() {
        $this->disponible = !$this->disponible;
    }
    public function updateNote(int $note) {
        $this->note = $note;
    }
}