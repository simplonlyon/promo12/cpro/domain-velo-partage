<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Location;
use App\Domain\Entity\Velo;

interface LocationRepositoryInterface {
    function add(Location $location):int;
    function find(int $id): Location;
    function update(Location $location);
    function findByVeloCurrent(int $idVelo):Location;
}