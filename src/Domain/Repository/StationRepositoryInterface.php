<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Station;

interface StationRepositoryInterface {
    function find(int $id): Station;
    function update(Station $location);
    function findAll(): array;
}