<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Utilisatrice;

interface UtilisatriceRepositoryInterface {
    function add(Utilisatrice $utilisatrice):int;
    function find(int $id): Utilisatrice;
    function update(Utilisatrice $location);
}