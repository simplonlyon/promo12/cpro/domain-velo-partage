<?php

namespace App\Domain\Repository;

use App\Domain\Entity\Location;
use App\Domain\Entity\Velo;

interface VeloRepositoryInterface {
    function find(int $id): Velo;
    function update(Velo $location);
}