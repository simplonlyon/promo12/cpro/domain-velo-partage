<?php


namespace App\Domain\UseCase;

use App\Domain\Entity\Location;
use App\Domain\Repository\LocationRepositoryInterface;
use App\Domain\Repository\StationRepositoryInterface;
use App\Domain\Repository\UtilisatriceRepositoryInterface;
use App\Domain\Repository\VeloRepositoryInterface;

class LocationUseCase implements LocationUseCaseInterface {
    
    private LocationRepositoryInterface $repoLocation;
    private StationRepositoryInterface $repoStation;
    private VeloRepositoryInterface $repoVelo;
    private UtilisatriceRepositoryInterface $repoUtilisatrice;

    public function __construct(LocationRepositoryInterface $repoLocation,  StationRepositoryInterface $repoStation, VeloRepositoryInterface $repoVelo, UtilisatriceRepositoryInterface $repoUtilisatrice) {
        $this->repoLocation =$repoLocation;
        $this->repoStation = $repoStation;
        $this->repoVelo = $repoVelo;
        $this->repoUtilisatrice = $repoUtilisatrice;
    }
    public function louerVelo(int $borne, int $idUtilisatrice, int $idStation): Location
    {
        $station = $this->repoStation->find($idStation);
        $utilisatrice = $this->repoUtilisatrice->find($idUtilisatrice);
        
        $velo = $station->retirerVelo($borne);
        $velo->toggleDisponible();

        $location = new Location($utilisatrice, $velo, new \DateTime());

        $this->repoLocation->add($location);
        $this->repoStation->update($station);
        $this->repoVelo->update($velo);
        return $location;
    }
    public function resituerVelo(int $borne, int $idStation, int $idVelo, int $note = 5, string $commentaire =null)
    {
        $station = $this->repoStation->find($idStation);
        $velo = $this->repoVelo->find($idVelo);
        
        $station->restituerVelo($velo, $borne);
        $velo->toggleDisponible();

        $location = $this->repoLocation->findByVeloCurrent($idVelo);
        $location->finirLocation(new \DateTime(), $note, $commentaire);
        $this->repoLocation->update($location);
        $this->repoStation->update($station);
        $this->repoVelo->update($velo);
    }
}