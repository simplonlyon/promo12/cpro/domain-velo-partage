<?php

namespace App\Domain\UseCase;

use App\Domain\Entity\Location;

interface LocationUseCaseInterface {
    function louerVelo(int $borne, int $idUtilisatrice, int $idStation):Location;
    function resituerVelo(int $borne, int $idStation, int $idVelo);
}